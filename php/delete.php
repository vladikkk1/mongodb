<?php

require_once '../vendor/autoload.php';
//Удаление кина полностью
$client = new \MongoDB\Client();
$collection = $client->kursova->cinema;
if (isset($_GET['name']) || !empty($_GET['name'])){
    $name = $_GET['name'];
}else{
    header("location:/admin");
}

$collection->deleteOne(['name'=>$name]);
$directory = $_SERVER['DOCUMENT_ROOT']."/img";
$name = $name.'.jpg';
if (file_exists($directory . '/' . $name)) {
    unlink($directory . '/' . $name);
}

header("location:/admin");