<?php
require_once '../vendor/autoload.php';
//Обновление данных кино
$client = new \MongoDB\Client();
$collection = $client->kursova->cinema;


// критерий отбора документов, которые надо обновить


if (isset($_POST['submit']) && !empty($_POST['name'])) {

    $name_old = $_POST['name_old'];
    $oldDocs = array("name" => "$name_old");


    $name = $_POST['name'];
    $genre = $_POST['genre'];
    $date = $_POST['date'];
    $time1 = $_POST['time1'];
    $time2 = $_POST['time2'];
    $time3 = $_POST['time3'];
    $director = $_POST['director'];
    $price = $_POST['price'];
    $photo = $_FILES['image']['name'];
    $photo = $name . '.jpg';
    // новый документ
    $newDoc = array(
        "name" => "$name",
        'genre' => $genre,
        'date' => $date,
        'time' => array(
            $time1, $time2, $time3
        ),
        'director' => $director,
        'price'=>$price,
        'photo' => $photo
    );


// дополнительные параметры обновления
// если документы по критерию не найдены, то новый документ вставляется
//    $option = array("upsert" => true);

    $collection->updateOne($oldDocs,['$set'=>$newDoc],['w'=>1]);
    $photo = $name.".jpg";
    $uploaddir = $_SERVER['DOCUMENT_ROOT']."/img";

    if(is_uploaded_file($_FILES['image']['tmp_name'])){
        move_uploaded_file($_FILES['image']['tmp_name'], "$uploaddir/$photo");

    }

    header("location:/admin");
}else{
    header("location:/admin");
}

