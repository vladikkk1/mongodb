<?php


require_once 'vendor/autoload.php';

session_start();
$client = new \MongoDB\Client();

//Создание коллекции подключение->название базы->название документа
$collection = $client->kursova->cinema;

//вывод данных с коллекции
$res = $collection->find()->toArray();
//echo "<pre>";
//var_dump($_SESSION['cart']);
//exit();
//unset($_SESSION['cart']);
?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.0/css/all.css" integrity="sha384-OLYO0LymqQ+uHXELyx93kblK5YIS3B2ZfLGBmsJaUyor7CpMTBsahDHByqSuWW+q" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;700&display=swap" rel="stylesheet">    <link rel="stylesheet" href="css/style.css">
    <title>Cinema</title>
</head>
<body>
    <header class="header">
        <div class="wrapper">
            <div class="header-content">
                <div style="display: flex ; align-items: center">
                    <div class="header-logo">

                    </div>
                    <div class="main-link">
                        <a href="/">Фільми</a>
                    </div>
                </div>
<!--                <div class="header-search">-->
<!--                    <input type="text">-->
<!--                    <i class="fas fa-search"></i>-->
<!--                </div>-->
                <div class="header-cart">
                    <a href="/cart"><i class="fas fa-shopping-cart"></i></a>
                </div>
            </div>
        </div>
    </header>
    <div class="wrapper">
        <h1 class="list-title">
            Cписок фільмів
        </h1>

        <div class="list">
            <? foreach ($res as $document):?>
            <div class="card">
                <div class="front">
                    <div class="movie-img" style="background: url('./img/<? echo $document['photo']?>') no-repeat center; background-size: cover;">

                    </div>
                </div>
                <div class="back">
                    <p class="movie-descr">Жанр : <? echo $document['genre'];?></p>
                    <p class="movie-descr">Режисер : <? echo $document['director'];?>і</p>
                    <p class="movie-date"><? echo $document['date'];?></p>
                    <div class="movie-times">
                        <?foreach ($document['time'] as $time):?>
                        <a href="/product/view.php?name=<? echo $document['name'];?>&date=<? echo $document['date'];?>&time=<? echo $time;?>"><? echo $time;?></a>
                        <?endforeach;?>
                    </div>
                </div>
                <p class="movie-name">
                    <? echo $document['name'];?>
                <p>
            </div>
            <?endforeach;?>




<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">14:00</a>-->
<!--                        <a href="">15:00</a>-->
<!--                        <a href="">16:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">14:00</a>-->
<!--                        <a href="">15:00</a>-->
<!--                        <a href="">16:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">14:00</a>-->
<!--                        <a href="">15:00</a>-->
<!--                        <a href="">16:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">14:00</a>-->
<!--                        <a href="">15:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">16:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">15:00</a>-->
<!--                        <a href="">16:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">14:00</a>-->
<!--                        <a href="">15:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
<!--            <div class="card">-->
<!--                <div class="front">-->
<!--                    <div class="movie-img" style="background: url('./img/123123.jpg') no-repeat center; background-size: cover;">-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="back">-->
<!--                    <p class="movie-descr">Жанр : комедія</p>-->
<!--                    <p class="movie-descr">Режисер : Тім Сторі</p>-->
<!--                    <p class="movie-date">20 березня - 28 березня</p>-->
<!--                    <div class="movie-times">-->
<!--                        <a href="">12:00</a>-->
<!--                        <a href="">14:00</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p class="movie-name">-->
<!--                    Головна подія-->
<!--                <p>-->
<!--            </div>-->
        </div>

    </div>

    <footer class="footer">
        <div class="wrapper">
            <div class="footer-content">
                <a href="/">Фільми</a>
                <div class="header-logo">

                </div>
                <a href="/cart">Кошик</a>
            </div>
        </div>
    </footer>
</body>
</html>