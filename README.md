MongoDB

Используется версия PHP 7.4, MongoDB v4.2.12 , OpenServer (last version), Composer (last version)

После установки Composer:
1. Настроить OpenServer под MongoDB (Настройки / Модули / MongoDB(4.2)).
2. Проверить наличие подключения extension = mongodb в конфигурационном файле сервера php_7.4.ini (дополнительно / Конфигурация / php_7.4) (OpenServer),
 если есть ;extension = mongodb, то убрать ; (привести к виду extension = mongodb )
3. В php_7.4.ini (OpenServer) добавить:
extension = php-mbstring
extension=php_extname.dll

Перезапустить OpenServer



