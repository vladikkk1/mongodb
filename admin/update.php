<?php

require_once '../vendor/autoload.php';
//Обновление данных кино
$client = new \MongoDB\Client();
$collection = $client->kursova->cinema;
if (isset($_GET['name']) || !empty($_GET['name'])){
    $name = $_GET['name'];
}else{
    header("location:/admin");
}
//var_dump($name);

$document = $collection->findOne(['name'=>$name]);
//$document = $collection->find()->toArray();
//var_dump($document);
//exit();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <title>Document</title>
</head>
<body>
<header style="padding: 10px; background: black;margin-bottom: 10px ">
    <div class="wrapper">
        <div class="nav">
            <a style="padding-right: 20px; color: white;" href="/order">Заказы</a>
            <a style="padding-right: 20px; color: white;" href="/admin">Фильмы</a>
            <a style="padding-right: 20px; color: white;" href="/">На сайт</a>
        </div>
    </div>
</header>

<div class="wrapper">
<form method="post" action="/php/update.php" enctype="multipart/form-data">
    <input class="form-control" type="hidden" name="name_old" value="<? echo $_GET['name'];?>">
    <input class="form-control" type="text" placeholder="Название фильма" value="<? echo $document['name'];?>" name="name"><br>
    <input class="form-control" type="text" placeholder="Жанр" value="<? echo $document['genre'];?>" name="genre"><br>
    <input class="form-control" type="date" placeholder="Дата" value="<? echo $document['date'];?>" name="date"><br>
    <label for="">Время сеансов</label>
    <div class="row">
        <div class="col-3">
    <select class="form-control" name="time1"  id="">
        <option value="<? echo $document['time'][0]?>"><? echo "Старое время:". $document['time'][0];?></option>
        <option value="12:00">12:00</option>
        <option value="13:00">13:00</option>
        <option value="14:00">14:00</option>
        <option value="15:00">15:00</option>
        <option value="16:00">16:00</option>
    </select><br>

    <select class="form-control" name="time2" id="">
        <option value="<? echo $document['time'][1]?>"><? echo "Старое время:". $document['time'][1];?></option>
        <option value="12:00">12:00</option>
        <option value="13:00">13:00</option>
        <option value="14:00">14:00</option>
        <option value="15:00">15:00</option>
        <option value="16:00">16:00</option>
    </select><br>

    <select class="form-control" name="time3" id="">
        <option value="<? echo $document['time'][2]?>"><? echo "Старое время:". $document['time'][2];?></option>
        <option value="12:00">12:00</option>
        <option value="13:00">13:00</option>
        <option value="14:00">14:00</option>
        <option value="15:00">15:00</option>
        <option value="16:00">16:00</option>
    </select>
            </div>
        </div>
    <br>
    <input class="form-control" type="text" value="<? echo $document['director'];?>" name="director"><br>
    <input class="form-control" type="text" value="<? echo $document['price'];?>"  name="price"><br>
    <? echo "Фото:". $document['photo'];?><br>
    <input class="form-control" style="border: 0" type="file" name="image" placeholder=""  value="<? echo $document['photo'];?>" multiple><br>

    <input class="form-control" type="submit" name="submit" value="submit">
</form>

</div>



<footer>

</footer>
</body>
</html>


