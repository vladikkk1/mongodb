<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <title>Document</title>
</head>
<body>
<header style="padding: 10px 0; background: black; ">
    <div class="wrapper">
        <div class="nav">
            <a style="padding-right: 20px; color: white;" href="/order">Заказы</a>
            <a style="padding-right: 20px; color: white;" href="/admin">Фильмы</a>
            <a style="padding-right: 20px; color: white;" href="/">На сайт</a>
        </div>
    </div>
</header>

<div class="wrapper">
<form method="post" action="/php/create.php" enctype="multipart/form-data">

    <input class="form-control" type="text" placeholder="Название фильма" name="name"><br>
    <input class="form-control" type="text" placeholder="Жанр" name="genre"><br>
    <input class="form-control" type="date" placeholder="Дата конца" name="date"><br>
    <label style="margin-bottom: 10px" for="">Время сеансов</label>
    <div class="row">
        <div class="col-3">
            <select  class="form-control" name="time1" id="">
                <option value="12:00">12:00</option>
                <option value="13:00">13:00</option>
                <option value="14:00">14:00</option>
                <option value="15:00">15:00</option>
                <option value="16:00">16:00</option>
            </select>
        </div>
        <div class="col-3">
            <select class="form-control" name="time2" id="">
                <option value="12:00">12:00</option>
                <option value="13:00">13:00</option>
                <option value="14:00">14:00</option>
                <option value="15:00">15:00</option>
                <option value="16:00">16:00</option>
            </select>
        </div>
        <div class="col-3">
            <select class="form-control" name="time3" id="">
                <option value="12:00">12:00</option>
                <option value="13:00">13:00</option>
                <option value="14:00">14:00</option>
                <option value="15:00">15:00</option>
                <option value="16:00">16:00</option>
            </select>
        </div>
    </div>

  <br>
    <input class="form-control" type="text" placeholder="Режисер" name="director"><br>
    <input class="form-control" type="text" placeholder="Цена за билет" name="price"><br>
    <input class="form-control" style="border: 0" type="file" name="image" placeholder="" value="" multiple><br>
    <input class="form-control" type="submit" name="submit" value="submit">
</form>

</div>

<footer>

</footer>
</body>

</html>


