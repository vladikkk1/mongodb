<?php

require_once '../vendor/autoload.php';
//Вывод данных в админке
$client = new \MongoDB\Client();
$collection = $client->kursova->cinema;

$res = $collection->find()->toArray();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <title>Document</title>
</head>
<body>
<header style="padding: 10px 0; background: black; ">
    <div class="wrapper">
        <div class="nav">
            <a style="padding-right: 20px; color: white;" href="/order">Заказы</a>
            <a style="padding-right: 20px; color: white;" href="/admin">Фильмы</a>
            <a style="padding-right: 20px; color: white;" href="/">На сайт</a>
        </div>
    </div>
</header>
<div class="wrapper">

<a style="display: block; padding: 5px 10px; background: black; border-radius: 5px; margin: 30px 0; width: 150px; color: white; text-align: center" href="/admin/create.php">Создать фильм</a>

<table class="table table-hover">

    <tr>
        <td>Название фильма</td>
        <td>Жанр</td>
        <td>Дата</td>
        <td>Время сеанса</td>
        <td>Режисер</td>
        <td>Цена за билет</td>
    </tr>


        <? foreach ($res as $document):?>
    <tr>
        <td><? echo $document['name'];?></td>
        <td><? echo $document['genre'];?></td>
        <td><? echo $document['date'];?></td>
            <td>
        <? foreach ($document['time'] as $key=>$value):?>
        <? echo $value;?>
        <?endforeach;?>
            </td>
        <td><? echo $document['director'];?></td>
        <td><? echo $document['price'];?></td>
        <td><a href="/admin/update.php?name=<? echo $document['name'];?>">Редактировать</a></td>
        <td><a href="/php/delete.php?name=<? echo $document['name'];?>">Удалить</a></td>
    </tr>
        <?endforeach;?>





</table>

</div>
</body>
<footer>

</footer>
</html>
