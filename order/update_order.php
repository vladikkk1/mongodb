<?php


require_once '../vendor/autoload.php';
$client = new \MongoDB\Client();

$collection = $client->kursova->orders;

if(isset($_POST['submit']) && !empty($_POST['name'])){

    $key = $_POST['key'];
    $name = $_POST['name'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $row = $_POST['row'];
    $place = $_POST['place'];
    $price = $_POST['price'];
    $order_id = $_POST['name_old'];
    $fio = $_POST['fio'];
    $email = $_POST['email'];

    $old = array(
        'name'=>$name,
        'date'=>$date,
        'time'=>$time,
        'row'=>$row,
        'place'=>$place,
        'price'=>$price,
        'fio'=>$fio,
        'email'=>$email
    );

    $collection->updateOne(array('_id' => new MongoDB\BSON\ObjectId($order_id)) ,array('$set'=>array("order.$key" => $old)));

//    header("location:/order/update.php?date=$date&id=$order_id&name=$name&row=$row&place=$place&time=$time");
    header("location:/order/");


}