<?php


require_once '../vendor/autoload.php';

session_start();
$client = new \MongoDB\Client();

//Создание коллекции подключение->название базы->название документа
$collection = $client->kursova->orders;

$order_id = $_GET['id'];


$res = (array)$collection->findOne(array('_id' => new MongoDB\BSON\ObjectId($order_id)));

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <title>Document</title>
</head>
<body>
<header style="padding: 10px 0; background: black; ">
    <div class="wrapper">
        <div class="nav">
            <a style="padding-right: 20px; color: white;" href="/order">Заказы</a>
            <a style="padding-right: 20px; color: white;" href="/admin">Фильмы</a>
            <a style="padding-right: 20px; color: white;" href="/">На сайт</a>
        </div>
    </div>
</header>
<div class="wrapper">
<p>Заказ # <? echo $order_id;?></p>
    <p>Клиент: <? echo $res['order'][0]['fio'];?></p>
    <p>Почта:<? echo $res['order'][0]['email'];?></p>
<table class="table table-hover">

    <tr>
        <td>Название фильма</td>
        <td>Дата</td>
        <td>Время</td>
<!--        <td>Ряд</td>-->
        <td>Место</td>
        <td>Цена</td>
        <td>Действие</td>

    </tr>

    <? foreach ($res['order'] as $document):?>
    <tr>
    <td><? echo $document['name'];?></td>
    <td><? echo $document['date'];?></td>
    <td><? echo $document['time'];?></td>
<!--    <td>--><?// echo $document['row'];?><!--</td>-->
    <td><? echo $document['place'];?></td>
    <td><? echo $document['price'];?></td>
    <td><a href="/order/update.php?date=<? echo $document['date'];?>&id=<? echo $order_id;?>&name=<? echo $document['name'];?>&row=<? echo $document['row'];?>&place=<? echo $document['place'];?>&time=<? echo $document['time'];?>">Редактировать</a></td>
    </tr>
    <?endforeach;?>


</table>


</div>


<footer>



</footer>
</body>
</html>
