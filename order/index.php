<?php

require_once '../vendor/autoload.php';
$client = new \MongoDB\Client();

$collection = $client->kursova->orders;

//вывод данных с коллекции
$res = $collection->find()->toArray();

foreach ($res as $key=>$value){

    $order_id[] = $value['_id'];
    $order = $value['order'];
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <title>Document</title>
</head>
<body>
<header style="padding: 10px 0; background: black; ">
    <div class="wrapper">
        <div class="nav">
            <a style="padding-right: 20px; color: white;" href="/order">Заказы</a>
            <a style="padding-right: 20px; color: white;" href="/admin">Фильмы</a>
            <a style="padding-right: 20px; color: white;" href="/">На сайт</a>
        </div>
    </div>
</header>
<div class="wrapper">
<h1>СТРАНИЦА ЗАКАЗОВ</h1>
<? if (isset($order_id)):?>
<table class="table table-hover">

    <tr>
     <td>Номер заказа</td>
     <td>Информация о нем</td>
     <td>Действие</td>
    </tr>

    <? foreach ($order_id as $key=>$id):?>
        <tr>
            <td><? echo $id;?></td>
            <td><a href="/order/view.php?id=<? echo $id;?>">Подробнее</a></td>
            <td><a href="/order/delete_all.php?id=<? echo $id;?>">Удалить</a></td>
        </tr>
    <?endforeach;?>
</table>
<? else:?>
<h4>Нет заказов</h4>
<?endif;?>
</div>



<footer>

</footer>
</body>
</html>


